package com.example.myapplication.config;

import android.app.Application;
import android.content.Context;

public class App extends Application {
    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
    }
    public Context getContext(){
        return appContext;
    }
    public static App getInstance() {
        return (App) appContext;
    }
}
