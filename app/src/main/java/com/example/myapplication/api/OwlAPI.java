package com.example.myapplication.api;

import com.example.myapplication.model.OwlData;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface OwlAPI {

    @GET("api/v2/json/BUMA-14590b/")
    Observable<OwlData> getSnowball();
}
