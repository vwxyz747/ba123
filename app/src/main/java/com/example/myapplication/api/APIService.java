package com.example.myapplication.api;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.example.myapplication.adapter.RvStockAdapter;
import com.example.myapplication.config.Constant;
import com.example.myapplication.model.OwlData;
import com.example.myapplication.model.Stock;
import com.example.myapplication.model.StockData;
import com.google.gson.JsonArray;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class APIService {
    public static APIService apiService;
    StockAPI stockAPI;
    OwlAPI owlAPI;

    public static APIService getInstance() {
        if (apiService == null) {
            apiService = new APIService();
        }
        return apiService;
    }

    public void fetchAPIData(io.reactivex.functions.Consumer<ArrayList<Stock>> consumer, Context context) {
        RetrofitController.getInstance().enableLoadingDialog(context);//開始loading
        stockAPI = RetrofitController.getInstance().getStockAPI();
        owlAPI = RetrofitController.getInstance().getOwlAPI();
        Observable.zip(stockAPI.getStockInfor(), owlAPI.getSnowball(), //把zip東西合併成新的型態
                (stockData, owlData) -> {
                    ArrayList<Stock> stocks = APIService.this.setStocks(stockData, owlData);
                    return stocks;//這邊不能return null,會沒法去下一段
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer
                , (Consumer<Throwable>) t -> Log.d(Constant.TAG + "1", t.toString()));
    }

    private ArrayList setStocks(StockData stockData, OwlData owlData) {
        ArrayList<Stock> list = new ArrayList<>();
        JsonArray array = stockData.getData();
        JsonArray owlDataArray = owlData.getData();
        int lastN = array.get(0).getAsJsonArray().size() - 1;//最後一個值是分數
        for (int i = 0; i < array.size(); i++) {
            JsonArray tmp = array.get(i).getAsJsonArray();
            Stock tmpStock = new Stock(tmp);
            list.add(tmpStock);
            for (int j = 0; j < owlDataArray.size(); j++) {
                String number = owlDataArray.get(j).getAsJsonArray().get(0).getAsString();
                if (number.equals(tmpStock.getNumber())) {
                    tmpStock.setScore(Integer.parseInt(owlDataArray.get(j).getAsJsonArray().get(lastN).getAsString()));
                    break;
                }
            }
        }
        return list;
    }
}
