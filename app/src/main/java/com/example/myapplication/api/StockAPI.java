package com.example.myapplication.api;

import com.example.myapplication.model.StockData;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;

public interface StockAPI {

    @GET("stock/stockprice")//API路徑
    Observable<StockData> getStockInfor();//取得資料轉換格式
}
