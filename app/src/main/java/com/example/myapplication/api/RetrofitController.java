package com.example.myapplication.api;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;

import androidx.appcompat.widget.DialogTitle;

import com.example.myapplication.R;
import com.example.myapplication.config.Constant;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogRecord;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static io.reactivex.android.BuildConfig.DEBUG;

public class RetrofitController {
    private static final String TAG = "RetrofitController";//logt
    private static RetrofitController retrofitController = new RetrofitController();
    private StockAPI stockAPI;
    private OwlAPI owlAPI;
    public static final String STOCK_BASE_URL = "https://money.lab2.cwmoney.net/";
    public static final String OWL_BASE_URL = "http://owl.cmoney.com.tw/OwlApi/";
    private static Dialog loadingBar;

    private RetrofitController() {

    }

    public static RetrofitController getInstance() {
        if (retrofitController == null) {
            retrofitController = new RetrofitController();
        }
        return retrofitController;
    }

    public RetrofitController enableLoadingDialog(Context context) {
        loadingBar = new Dialog(context);
        loadingBar.setCancelable(false);
        loadingBar.setContentView(LayoutInflater.from(context).inflate(R.layout.loading_dialog, null));
        loadingBar.show();
        return this;
    }

    public void loadingDialogDismiss() {
        if (loadingBar != null) {
            loadingBar.dismiss();
        }
    }

    public StockAPI getStockAPI() {
        if (stockAPI == null) {
            initStockAPI();
        }
        return stockAPI;
    }

    public void initStockAPI() {
        // 設置baseUrl=>要連的網站，addConverterFactory用Gson作為資料處理Converter
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(STOCK_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        stockAPI = retrofit.create(StockAPI.class);
    }


    public OwlAPI getOwlAPI() {
        if (owlAPI == null) {
            initOwlAPI();
        }
        return this.owlAPI;
    }

    private void initOwlAPI() {
        OkHttpClient client = new OkHttpClient()
                .newBuilder()
                .connectTimeout(7, TimeUnit.SECONDS)   // 設置連線Timeout
                .authenticator((route, response) -> {
                    String token = getToken();//拿token
                    return response.request().newBuilder()
                            .addHeader("Authorization", "Bearer " + token)
                            .build();
                })
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(OWL_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        owlAPI = retrofit.create(OwlAPI.class);
    }

    private String getToken() {
        String ROUTE = "auth";
        String appId = "20180828095858963";
        String appSecret = "ed9bce30aa6511e88f68000c29cc8594";//拿貓頭鷹Token
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, "appId=" + appId + "&appSecret=" + appSecret);
        Request request = new Request.Builder()
                .url(OWL_BASE_URL + ROUTE)
                .post(body)
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .build();
        String token = "";
        try {
            Response response = client.newCall(request).execute();
            try {
                token = new JSONObject(response.body().string()).getString("token");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(Constant.TAG, token);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return token;
    }
}
