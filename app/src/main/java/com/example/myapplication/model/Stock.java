package com.example.myapplication.model;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.example.myapplication.R;
import com.example.myapplication.config.App;
import com.example.myapplication.config.Constant;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DecimalFormat;

public class Stock extends BaseObservable {

    private String number;
    private String name;
    private String date;
    private double startP;
    private String highestP;
    private String lowestP;
    private String maxP;
    private String minP;
    private String tradeAmount;
    private String tradeSum;
    private String bestBuy;
    private String bestSell;
    private String yesterdayP;
    private String referP;
    private double instantPrice;//參考價
    private double changePrice;
    private double changePercent;
    private int score = 0;
    private String signalText;
    private int snowballImg;
    private int signalColor;
    private int color;//綠還紅還白
    private int arrow;

    //     "股票代號",
//             "股票名稱",
//             "日期",
//             "開盤價",
//             "最高價",
//             "最低價",
//             "漲停價",
//             "跌停價",
//             "成交量",
//             "成交總量",
//             "最佳買價",
//             "最佳賣價",
//             "昨收",
//             "參考價",
//             "成交價"
    public Stock(String name, String num, double instantPrice, double changePrice, int score) {
        this.name = name;
        this.number = num;
        this.instantPrice = instantPrice;
        this.changePrice = changePrice;
        this.startP = instantPrice - changePrice;
        this.changePercent = (changePrice / startP);
        this.score = score;
        if (changePrice == 0) {

        } else if (changePrice > 0) {

        } else {

        }
    }

    public Stock(JsonArray data) {
        int i = 0;
        try {
            number = data.get(i++).getAsString();
            name = data.get(i++).getAsString();
            date = data.get(i++).getAsString();
            startP = Double.parseDouble(data.get(i++).getAsString());
            highestP = data.get(i++).getAsString();
            lowestP = data.get(i++).getAsString();
            maxP = data.get(i++).getAsString();
            minP = data.get(i++).getAsString();
            tradeAmount = data.get(i++).getAsString();
            tradeSum = data.get(i++).getAsString();
            bestBuy = data.get(i++).getAsString();
            bestSell = data.get(i++).getAsString();
            try {
                yesterdayP = data.get(i++).getAsString();//1213是null就跳catch
            } catch (Exception e) {
yesterdayP="0";
            }
            referP = data.get(i++).getAsString();
            instantPrice = Double.parseDouble(data.get(i++).getAsString());
        } catch (UnsupportedOperationException e) {

        }
        this.changePrice = instantPrice - Double.parseDouble(yesterdayP);
        this.changePercent = (changePrice / Double.parseDouble(yesterdayP));
        getChangePrice();//連動的東西更新！
    }

    public Stock(JSONArray data) {
        try {
            int i = 0;
            number = data.getString(i++);
            name = data.getString(i++);
            date = data.getString(i++);
            startP = Double.parseDouble(data.getString(i++));
            highestP = data.getString(i++);
            lowestP = data.getString(i++);
            maxP = data.getString(i++);
            minP = data.getString(i++);
            tradeAmount = data.getString(i++);
            tradeSum = data.getString(i++);
            bestBuy = data.getString(i++);
            bestSell = data.getString(i++);
            yesterdayP = data.getString(i++);
            referP = data.getString(i++);
            instantPrice = Double.parseDouble(data.getString(i++));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.changePrice = instantPrice - startP;
        this.changePercent = (changePrice / startP);
    }

    @Bindable
    public String getName() {
        return this.name;
    }

    @Bindable
    public String getNumber() {
        return this.number;
    }

    @Bindable
    public String getChangePrice() {
        if (changePrice == 0) {
            return "-";
        }
//        if (changePrice > 0) {
//            this.color = R.color.upRed;
//            this.arrow = R.drawable.up_arrow;
//        } else if (changePrice == 0) {
//            this.color = R.color.white;
//            this.arrow = 0;
//            return "-";
//        } else {
//            this.color = R.color.downGreen;
//            this.arrow = R.drawable.down_arrow;
//        }
        DecimalFormat df = new DecimalFormat("#.##");
        String s = df.format(this.changePrice);
        return s;
    }

    @Bindable
    public double getInstantPrice() {
        return this.instantPrice;
    }

    @Bindable
    public int getColor() {
        if (color == 0) {
            color = R.color.white;
        }
        if (changePrice > 0) {
            this.color = R.color.upRed;
        } else if (changePrice == 0) {
            this.color = R.color.white;
        } else {
            this.color = R.color.downGreen;
        }
        return App.getInstance().getColor(color);
    }

    @Bindable
    public String getChangePercentStr() {
        if (changePercent < 0.01 && changePercent > 0) {
            return "(0.01%)";
        } else if (changePercent > -0.01 && changePercent < 0) {
            return "(-0.01%)";
        }
        DecimalFormat df = new DecimalFormat("#.##");
        String s = df.format(this.changePercent);
        return "(" + s + "%)";
    }

    public String getSignalText() {
        return signalText;
    }

    @Bindable
    public int getSignalColor() {
        if (signalColor == 0) {
            return App.getInstance().getColor(R.color.black);
        }
        return App.getInstance().getColor(signalColor);
    }

    @Bindable
    public Drawable getSnowballDrawable() {
        if (snowballImg == 0) {
            return App.getInstance().getDrawable(R.drawable.bg_snow_state1);
        }
        return App.getInstance().getDrawable(snowballImg);
    }

    @Bindable
    public Drawable getArrowDrawable() {
        if (changePrice > 0) {
            this.arrow = R.drawable.up_arrow;
        } else if (changePrice == 0) {
            return null;
        } else {
            this.arrow = R.drawable.down_arrow;
        }
        return App.getInstance().getDrawable(arrow);
    }

    @Bindable
    public int getScore() {
        return this.score;
    }

    private void updateScore() {
        if (score > 70) {
            this.signalText = "優\n良";
            this.signalColor = R.color.wellSignal;
            this.snowballImg = R.drawable.bg_snow_state3;
        } else if (score > 40) {
            this.signalText = "中\n立";
            this.signalColor = R.color.black;
            this.snowballImg = R.drawable.bg_snow_state2;
        } else {
            this.signalText = "危\n險";
            this.signalColor = R.color.dangerSignal;
            this.snowballImg = R.drawable.bg_snow_state1;
        }
    }

    public void setScore(int score) {
        this.score = score;
        updateScore();//set後更新連動圖片顏色數據
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
