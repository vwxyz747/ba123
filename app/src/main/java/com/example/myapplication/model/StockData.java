package com.example.myapplication.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class StockData{
    private JsonArray Data;
    private JsonArray Title;

    //接收股票ＡＰＩ
    public StockData(JsonArray Title, JsonArray Data) {
        this.Title = Title;
        this.Data = Data;
    }

    public JsonArray getData() {
        return Data;
    }

    public void setData(JsonArray data) {
        Data = data;
    }

    public JsonArray getTitle() {
        return Title;
    }

    public void setTitle(JsonArray title) {
        Title = title;
    }
}
