package com.example.myapplication.model;

import com.google.gson.JsonArray;

public class OwlData {
    private JsonArray Data;

    private JsonArray Title;

    //接收雪球資料
    public OwlData(JsonArray data) {
        Data = data;
    }

    public JsonArray getData() {
        return Data;
    }

    public void setData(JsonArray data) {
        Data = data;
    }

    public JsonArray getTitle() {
        return Title;
    }

    public void setTitle(JsonArray title) {
        Title = title;
    }
}
