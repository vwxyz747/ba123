package com.example.myapplication.adapter;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.config.Constant;
import com.example.myapplication.databinding.StockItemBinding;
import com.example.myapplication.model.Stock;

import java.util.ArrayList;

public class RvStockAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<Stock> stocks;
    final int STOCK_VIEW = 1;
    final int TEST_VIEW = 2;

    public RvStockAdapter(ArrayList<Stock> stocks) {
        this.stocks = stocks;
    }

    public RvStockAdapter() {
    }

    private class StockHolder extends RecyclerView.ViewHolder {
        private StockItemBinding binding;

        public StockHolder(@NonNull View itemView) {
            super(itemView);
            this.binding = StockItemBinding.bind(itemView);
        }

        public void bind(Object obj) {
            binding.setVariable(com.example.myapplication.BR.data, obj);
            binding.executePendingBindings();
        }
    }

    private class TestHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public TestHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView2);
        }
    }

    public void renewList(ArrayList<Stock> stocks) {
        this.stocks = stocks;
        this.notifyDataSetChanged();//通知大家更新
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case STOCK_VIEW:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_item, parent, false);
                return new StockHolder(view);
            case TEST_VIEW:
                View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_item, parent, false);
                return new TestHolder(view2);
            default:
                View view3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_item, parent, false);
                return new StockHolder(view3);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return STOCK_VIEW;
        } else {
            return TEST_VIEW;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof RecyclerView.ViewHolder) {
            int type=getItemViewType(position);
            if (type == STOCK_VIEW) {

                Stock tmp = stocks.get(position);
                ((StockHolder) holder).bind(tmp);

            } else if (type == TEST_VIEW) {
                ((TestHolder)holder).textView.setText(String.valueOf(position));
            }
        }
    }

    @Override
    public int getItemCount() {
        if (stocks == null) {
            return 0;
        }
        return stocks.size();
    }
}
