package com.example.myapplication.viewController;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.adapter.RvStockAdapter;
import com.example.myapplication.api.APIService;
import com.example.myapplication.api.OwlAPI;
import com.example.myapplication.api.RetrofitController;
import com.example.myapplication.api.StockAPI;
import com.example.myapplication.config.Constant;
import com.example.myapplication.model.OwlData;
import com.example.myapplication.model.Stock;
import com.example.myapplication.model.StockData;
import com.google.gson.JsonArray;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    RecyclerView rvList;
    RvStockAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        rvList = findViewById(R.id.rv_stockList);
        rvList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RvStockAdapter();
        rvList.setAdapter(adapter);
        fetchData();
    }

    private void fetchData() {
         io.reactivex.functions.Consumer<ArrayList<Stock>> consumer=new Consumer<ArrayList<Stock>>() {
            @Override
            public void accept(ArrayList<Stock> stocks) throws Exception {
                Handler handler = new Handler();
                handler.post(() -> {
                    adapter.renewList(stocks);//更新list
                });
                RetrofitController.getInstance().loadingDialogDismiss();
            }
        };
        APIService.getInstance().fetchAPIData(consumer,this);
    }
}